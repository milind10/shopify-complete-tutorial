const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const { Sequelize, DataTypes } = require("sequelize");
const { default: axios } = require('axios');
const sequelize = new Sequelize('restful_db', 'milind', 'password', {
    host: 'localhost',
    dialect: 'mysql'
});
sequelize.authenticate()
    .then(() => {
        console.log("connection established");
    })
    .catch((err) => {
        console.log("error encountered");
    });
const store = sequelize.define('store', {
    storeID: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    domain: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timeZone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    moneyFormat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    customerEmail: {
        type: DataTypes.STRING,
        allowNull: false
    },
    myShopifyDomain: {
        type: DataTypes.STRING,
        allowNull: false
    },
    accessToken: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});
store.sync()
    .then((data) => {
        console.log("Stores table created");
    })
    .catch((err) => {
        console.log("Table was not created");
    });
const shopify_product = sequelize.define('shopify_product', {
    product_id: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    variant_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true
    },
    product_title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    variant_title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    product_image: {
        type: DataTypes.STRING,
        allowNUll: false
    },
    variant_image: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    timestamps: false
});
shopify_product.sync()
    .then((data) => {
        console.log("shopify_product table created");
    })
    .catch((error) => {
        console.log("shopify_product table was not created");
    });
//api to create new products
app.post('/api/product/createNewProduct/:shopDomain', (req, res) => {
    store.findOne({
        where: {
            domain: req.params.shopDomain
        }
    })
        .then((data) => {
            console.log(data.accessToken);
            const accessToken = data.accessToken;
            const producturl = `https://${req.params.shopDomain}/admin/api/2021-10/products.json`;
            const myProduct = {
                product: req.body.product
            };
            console.log(myProduct);
            axios.post(producturl, myProduct, { headers: { 'X-Shopify-Access-Token': data.accessToken } })
                .then((data) => {
                    console.log("data is ", data.data);
                    const productimageid = data.data.product.image.id;
                    const productTitle = data.data.product.title;
                    console.log("Product image id is", productimageid);
                    console.log("Imgaes are", data.data.product.images);
                    console.log("Variants are", data.data.product.variants);
                    for (let i = 0; i < data.data.product.images.length; i++) {
                        const variantid = 0;
                        axios.put(`https://${req.params.shopDomain}/admin/api/2021-10/variants/${data.data.product.variants[i].id}.json`, { variant: { "id": `${data.data.product.variants[i].id}`, "image_id": `${data.data.product.images[i].id}` } }, {
                            headers: { 'X-Shopify-Access-Token': accessToken }
                        })
                            .then((data) => {
                                console.log("Variant updated", data.data.variant);
                                console.log("Variant's image id is", data.data.variant.image_id);
                                const variantimageid = data.data.variant.image_id;
                                const variantid = data.data.variant.id;
                                const variantTitle = data.data.variant.title;
                                const productid = data.data.variant.product_id;
                                axios.get(`https://securecod4.myshopify.com/admin/api/2021-10/products/${productid}/images/${variantimageid}.json`, {
                                    headers: { 'X-Shopify-Access-Token': accessToken }
                                })
                                    .then((data) => {
                                        const variantimageurl = data.data.image.src;
                                        axios.get(`https://securecod4.myshopify.com/admin/api/2021-10/products/${productid}/images/${productimageid}.json`, {
                                            headers: { 'X-Shopify-Access-Token': accessToken }
                                        })
                                            .then((data) => {
                                                console.log("Product image url is", data.data.image.src);
                                                console.log("Urls are ", variantimageurl);
                                                const newProduct = {
                                                    product_id: productid,
                                                    variant_id: variantid,
                                                    product_title: productTitle,
                                                    variant_title: variantTitle,
                                                    product_image: data.data.image.src,
                                                    variant_image: variantimageurl
                                                };
                                                shopify_product.create(newProduct)
                                                    .then((data) => {
                                                        console.log("Successfully done");
                                                    })
                                                    .catch((error) => {
                                                        console.log("Not done yet", error);
                                                    });
                                            })
                                            .catch((error) => {
                                                console.log("Error while fetching product image url ", error);
                                            })
                                    })
                                    .catch((error) => {
                                        console.log("Not got urls", error);
                                    })
                            })
                            .catch((error) => {
                                console.log("Variant was not updated", error);
                            });
                    }
                })
                .catch((error) => {
                    console.log("error is ", error);
                })
            res.send(data.accessToken);
        })
        .catch((error) => {
            console.log("Error is", error);
            res.send(error);
        })
});
//api to update the title of existing product
app.put('/api/products/update/:productId/:shopDomain', (req, res) => {
   const shopDomain = req.params.shopDomain;
   const productId = req.params.productId;
   console.log(shopDomain,productId);
   const newTitle = req.body.newTitle;
   store.findOne({
       where :{
           domain  : shopDomain
       }
   })
   .then((data)=>{
       console.log("Got access Token", data.dataValues.accessToken);
       const accessToken = data.dataValues.accessToken;
       const puturl = `https://${shopDomain}/admin/api/2021-10/products/${productId}.json`;
       axios.put(puturl,{product:{"id":`${productId}`,"title":`${newTitle}`}},{headers :{
           'X-Shopify-Access-Token' : accessToken
       }})
       .then((data)=>{
           console.log("Title Updated",data);
           shopify_product.update({product_title :`${newTitle}`},{
               where :{
                   product_id : `${productId}`
               }
           })
           .then((data)=>{
               console.log("Product title updated in database");
           })
           .catch((error)=>{
               console.log("product title was not updated in the database");
           })
           res.send("Title Updated");
       })
       .catch((error)=>{
           console.log("Title not updated",error);
           res.send("Title not updated");
       })
       console.log("put url is",puturl);
   })
   .catch((error)=>{
       res.send(error);
   })
})
//api to delete product from store
app.delete('/api/products/delete/:productId/:shopDomain',(req,res)=>{
    const shopDomain = req.params.shopDomain;
    const productId = req.params.productId;
    store.findOne({
        where:{
            domain : shopDomain
        }
    })
    .then((data)=>{
        const accessToken = data.dataValues.accessToken;
        axios.delete(`https://${shopDomain}/admin/api/2021-10/products/${productId}.json`,{
            headers:{
                'X-Shopify-Access-Token' :`${accessToken}`
            }
        })
        .then((data)=>{
            console.log("Product deleted from store");
        })
        .catch((error)=>{
            console.log("Porduct was not deleted",error);
        })
    })
});
app.listen(3000, () => {
    console.log("Products server is running at port 3000");
});