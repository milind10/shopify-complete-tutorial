const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const { Sequelize, DataTypes } = require("sequelize");
const { default: axios } = require('axios');
const sequelize = new Sequelize('restful_db', 'milind', 'password', {
    host: 'localhost',
    dialect: 'mysql'
});
sequelize.authenticate()
    .then(() => {
        console.log("connection established");
    })
    .catch((err) => {
        console.log("error encountered");
    });
const store = sequelize.define('store', {
    storeID: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    domain: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timeZone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    moneyFormat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    customerEmail: {
        type: DataTypes.STRING,
        allowNull: false
    },
    myShopifyDomain: {
        type: DataTypes.STRING,
        allowNull: false
    },
    accessToken: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});
store.sync()
    .then((data) => {
        console.log("Stores table created");
    })
    .catch((err) => {
        console.log("Table was not created");
    });
//api to retreive collection detail of particular collection 
app.get('/api/collection/:collectionId/:shopDomain', (req, res) => {
    const collectionId = req.params.collectionId;
    const shopDomain = req.params.shopDomain;
    store.findOne({
        where: {
            domain: shopDomain
        }
    })
        .then((data) => {
            const accessToken = data.dataValues.accessToken;
            console.log("Got access Token ", accessToken);
            const geturl = `https://${shopDomain}/admin/api/2021-10/collections/${collectionId}.json`;
            axios.get(geturl, {
                headers: {
                    'X-Shopify-Access-Token': `${accessToken}`
                }
            })
                .then((data) => {
                    console.log("Collection detail is", data.data);
                })
                .catch((error) => {
                    console.log("error while fetching collection details", error);
                })
            res.send(geturl);
        })
        .catch((error) => {
            console.log("Caught an error while fetching access token", error);
            res.send("access token error", error);
        });
});
//api to get product details in the collection
app.get('/api/collection/listProducts/:collectionId/:shopDomain', (req, res) => {
    const shopDomain = req.params.shopDomain;
    const collectionId = req.params.collectionId;
    store.findOne({
        where: {
            domain: shopDomain
        }
    })
        .then((data) => {
            const accessToken = data.dataValues.accessToken;
            const geturl = `https://${shopDomain}/admin/api/2021-10/collections/${collectionId}/products.json`;
            axios.get(geturl, {
                headers: {
                    'X-Shopify-Access-Token': `${accessToken}`
                }
            })
                .then((data) => {
                    console.log("List of products is", data.data);
                })
                .catch((error) => {
                    console.log("Caught error while fetching products list in the collection", error);
                })
            res.send(geturl);
        })
        .catch((error) => {
            res.send(error);
        })
});
//api to create custom coollection 
app.post('/api/collection/createCustomCollection/:shopDomain', (req, res) => {
    const shopDomain = req.params.shopDomain;
    const collectiontitle = req.body.title;
    store.findOne({
        where: {
            domain: shopDomain
        }
    })
        .then((data) => {
            const accessToken = data.dataValues.accessToken;
            console.log("Access Token is", accessToken);
            const posturl = `https://${shopDomain}/admin/api/2021-10/custom_collections.json`;
            axios.post(posturl, { custom_collection: { "title": `${collectiontitle}` } }, {
                headers: {
                    'X-Shopify-Access-Token': `${accessToken}`
                }
            })
                .then((data) => {
                    console.log("Custom collection successfully created");
                })
                .catch((error) => {
                    console.log("Custom collection was not created");
                })
            res.send(posturl);
        })
        .catch((error) => {
            res.send("Not got access token");
        })
})
app.listen(3000, () => {
    console.log("Server started at port 3000");
})