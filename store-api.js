const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const { Sequelize, DataTypes } = require("sequelize");
const sequelize = new Sequelize('restful_db', 'milind', 'password', {
    host: 'localhost',
    dialect: 'mysql'
});
sequelize.authenticate()
    .then(() => {
        console.log("connection established");
    })
    .catch((err) => {
        console.log("error encountered");
    });
const store = sequelize.define('store', {
    storeID: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    domain: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timeZone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    moneyFormat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    customerEmail: {
        type: DataTypes.STRING,
        allowNull: false
    },
    myShopifyDomain: {
        type: DataTypes.STRING,
        allowNull: false
    },
    accessToken: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});
store.sync()
    .then((data) => {
        console.log("Stores table created");
    })
    .catch((err) => {
        console.log("Table was not created");
    });
app.post('/api/store/addNew', (req, res) => {
    const object = {
        storeID: req.body.id,
        domain: req.body.domain,
        country: req.body.country,
        timeZone: req.body.timezone,
        phoneNumber: req.body.phone,
        moneyFormat: req.body.moneyformat,
        customerEmail: req.body.customeremail,
        myShopifyDomain: req.body.myshopifydomain,
        accessToken: req.body.accesstoken
    };
    store.create(object)
    .then((data)=>{
        console.log("Data successfully inserted");
    })    
    .catch((err)=>{
        console.log("Data was not inserted");
    })

});
app.put('/api/store/update/:id',(req,res)=>{
    const object = {
        storeID: req.body.id,
        domain: req.body.domain,
        country: req.body.country,
        timeZone: req.body.timezone,
        phoneNumber: req.body.phone,
        moneyFormat: req.body.moneyformat,
        customerEmail: req.body.customeremail,
        myShopifyDomain: req.body.myshopifydomain,
        accessToken: req.body.accesstoken
    };
    store.update(object,{
        where :{
            storeID: req.params.id
        }
    })
    .then((data)=>{
        console.log("Data successfully updated");
    })
    .catch((err)=>{
        console.log("Data was not updated");
    })
});

app.get('/api/store/fetchSpecificStore/:id', (req, res) => {
    store.findAll({
        where: {
            storeID: req.params.id
        }
    })
        .then((data) => {
            res.send(JSON.stringify({data}));
        })
        .catch((err) => {
            res.send(JSON.stringify({ "status": 404 }));
        })
});
app.get('/api/store/:id/try',(req,res)=>{
    res.send({parameter : req.params.id});
})
app.listen(3000,(req,res)=>{
    console.log("Server created at port 3000");
})