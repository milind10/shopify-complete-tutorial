const express = require("express");
const crypto = require("crypto");
const axios = require('axios');
const nonce = require('nonce')();
const request = require('request-promise');
const querystring = require('querystring');
const cookie = require('cookie');
const app = express();
app.get('/login', (req, res) => {
    try {
        const shopName = req.query.shop_name;
        console.log("Inside login", shopName);
        if (shopName) {
            //shopify callback redirect
            const redirectURL = "http://localhost:5000/auth/callback";
            const api_key = "198f7cc06f636154ee2fa186e6bd922e";
            const scopes = "write_orders,read_orders,write_products,read_products";
            const state = 12345;
            //Install URL for app install 
            const shopifyURL = `https://${shopName}.myshopify.com/admin/oauth/authorize?client_id=${api_key}&scope=${scopes}&redirect_uri=${redirectURL}&state=${state}`;
            res.redirect(shopifyURL);
        }
        else {
            return res.status(400).send("Missing shop name parameter");
        }
    } catch (error) {
        console.log("Error inside login", error);
    }
});
app.get('/auth/callback', (req, res) => { 
    try {
        let { hmac, ...params } = req.query;
        console.log("hmac", hmac);
        const api_key = "198f7cc06f636154ee2fa186e6bd922e";
        console.log("params", params);
        const message = querystring.stringify(params);
        const shopName = req.query.shop;
        console.log("Shop name inside auth/callback ", shopName)
        const secret_Api = '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';
        const newhmac = crypto.createHmac('sha256', secret_Api).update(message).digest('hex');
        console.log(req.query);
        if (newhmac === req.query.hmac) {
            const accesstokenrequest = `https://${shopName}/admin/oauth/access_token`;
            const accessTokenPayload = {
                client_id: api_key,
                client_secret: secret_Api,
                code: req.query.code
            };
            axios.post(
                `https://${shopName}/admin/oauth/access_token`,                         //step 5 in OAuth process (App requests for the access token to shopify)
                accessTokenPayload
            )
                .then((response) => {
                    console.log("Data we got after making post request", response.data.access_token);
                    const access = response.data.access_token;
                    console.log("Access token is",access);
                    const requestURL = `https://${shopName}/admin/api/2021-10/shop.json`;
                    axios.get(requestURL, {
                        headers:
                        {
                            'X-Shopify-Access-Token': response.data.access_token
                        }
                    })
                    .then((data)=>{
                        console.log("Data is data",data);
                        const entry = {
                            id: data.data.shop.id,
                            domain: data.data.shop.domain,
                            country: data.data.shop.country,
                            timezone: data.data.shop.timezone,
                            phone: data.data.shop.phone,
                            moneyformat: data.data.shop.money_format,
                            customeremail: data.data.shop.customer_email,
                            myshopifydomain: data.data.shop.myshopify_domain,
                            accesstoken: access
                        }
                        console.log(entry);
                        axios.get(`http://localhost:3000/api/store/fetchSpecificStore/${data.data.shop.id}`)
                        .then((response)=>{
                            if(response.data.length > 0){
                                axios.put(`http://localhost:3000/api/store/update/${data.data.shop.id}`,entry)
                                .then((data)=>{
                                    console.log("Data was updated");
                                })
                                .catch((err)=>{
                                    console.log("Data was not updated");
                                })
                            }
                            else{
                                axios.post('http://localhost:3000/api/store/addNew',entry)
                                   .then((data)=>{
                                       console.log("Data successfully inserted");
                                   })
                                   .catch((err)=>{
                                       console.log("Caught an error while inserting the data");
                                   })
                            }
                        })
                        .catch((error)=>{
                            console.log("Not getting what to write");
                        })                     
                    })
                    .catch((error)=>{
                        console.log("Error is",error);
                    })
                })
                .catch((error)=>{
                    console.log("Caught an error while fetching access token");
                })
        }
    } catch (error) {
        console.log("Cannot go into auth /callback");
    }
});
app.listen(5000,()=>{
    console.log("Server started at port 5000");
})