const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const { Sequelize, DataTypes } = require("sequelize");
const { default: axios } = require('axios');
const { get } = require('request');
const sequelize = new Sequelize('restful_db', 'milind', 'password', {
    host: 'localhost',
    dialect: 'mysql'
});
sequelize.authenticate()
    .then(() => {
        console.log("connection established");
    })
    .catch((err) => {
        console.log("error encountered");
    });
const shopify_customers = sequelize.define('shopify_customers', {
    customer_id: {
        type: DataTypes.BIGINT,
        primaryKey: true
    },
    customer_email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    },
    customer_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    customer_orders_count: {
        type: DataTypes.BIGINT,
        allowNull: true
    },
    customer_last_order_id: {
        type: DataTypes.BIGINT,
        allowNull: true
    }
}, {
    timestamps: false
});
shopify_customers.sync({ alter: true })
    .then((data) => { console.log("shopify_customers table created"); })
    .catch((error) => { console.log("shopify_customers table was not created") });
const store = sequelize.define('store', {
    storeID: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    domain: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timeZone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    moneyFormat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    customerEmail: {
        type: DataTypes.STRING,
        allowNull: false
    },
    myShopifyDomain: {
        type: DataTypes.STRING,
        allowNull: false
    },
    accessToken: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});
store.sync()
    .then((data) => {
        console.log("Stores table created");
    })
    .catch((err) => {
        console.log("Table was not created");
    });
app.get('/api/customers/getAll/:shopDomain', (req, res) => {
    const shopDomain = req.params.shopDomain;
    store.findOne({
        where: {
            domain: shopDomain
        }
    })
        .then(async (data) => {
            const accessToken = data.dataValues.accessToken;
            let url = `https://${shopDomain}/admin/api/2021-10/customers.json?limit=10`;
            //console.log("Geturl is", url);

            while (url) {
                url = await callShopify(url, accessToken);
                console.log("Next URL " + url);
                if (url != undefined) {
                    const response = await axios.get(url, { headers: { 'X-Shopify-Access-Token': accessToken } });
                    console.log("Response we got is", response.data.customers);
                    console.log("Numbers are", response.data.customers.length);
                    let totalcustomers = response.data.customers.length;
                    for (let i = 0; i < totalcustomers; i++) {
                        const meta = response.data.customers[i];
                        console.log("Order counts of the customer are", meta.orders_count);
                        console.log(typeof meta.orders_count);
                        console.log("Last order id is ", meta.last_order_id);
                        console.log(typeof meta.last_order_id);
                        const consumer = {
                            customer_id: meta.id,
                            customer_email: meta.email,
                            customer_name: meta.first_name,
                            customer_orders_count: meta.orders_count,
                            customer_last_order_id: meta.last_order_id
                        };
                        console.log("New Customer is", consumer);
                        shopify_customers.create(consumer)
                            .then((data) => { console.log("Data inserted"); })
                            .catch((error) => { console.log("Data was not inserted"); });

                    }
                }
            }
        })
        .catch((error) => {
            console.log("Something is fishy", error);
        })
})
app.get('/api/getCustomers/:perpage/:pagenumber', async (req, res) => {
    const newoffset = Number((req.params.pagenumber - 1) * req.params.perpage);
    const newlimit = Number(req.params.perpage);
    const data = await shopify_customers.findAll()
    console.log("Total length", data.length);
    if (newoffset >= data.length) {
        res.send(JSON.stringify({ status: 404, error: "resource not found" }));
    }
    else {
        const data = await shopify_customers.findAll({ offset: newoffset, limit: newlimit });
        let records = new Array();
        for (let i = 0; i < data.length; i++) {
            records.push(data[i].dataValues);
        }
        const response = {
            per_page: newlimit,
            status_code: 200,
            customers: records,
            page_number: Number(req.params.pagenumber)
        };
        res.send(JSON.stringify(response));
    }
})
const parseNextUrlFromLink = (url) => {
    let previous = 'rel="previous", <';
    let startingposition = url.search(previous);
    let next = '>; rel="next"';
    let endingposition = url.search(next);
    if (startingposition != -1 && endingposition != -1)//link is present between these two position
    {
        startingposition = startingposition + previous.length;
        const newurl = url.substring(startingposition, endingposition);
        return newurl;
    }
    else if (startingposition != -1 && endingposition == -1)//only previous link is present
    {
        const newurl = '';
        return newurl;
    }
    else if (startingposition == -1 && endingposition != -1)//only next link is present
    {
        const newurl = url.substr(1, endingposition - 1);
        return newurl;
    }
}
const callShopify = async (mainurl, accessToken) => {
    try {
        const response = await axios.get(mainurl, { headers: { 'X-Shopify-Access-Token': `${accessToken}` } });
        const url = parseNextUrlFromLink(response.headers["link"]);
        return url;
    } catch (error) {
        console.log("Something fishy.......");
        console.log(error);
    }
}

// })
app.listen(3000, () => {
    console.log("Server started at port 3000");
})